package com.example.week7new;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

// Extend HttpServlet class
public class HelloForm extends HttpServlet {


    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        String title = "Using GET Method to Read Form Data";
        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " +
                        "transitional//en\">\n";

        out.println(docType +
                        "<html>\n" +
                        "<head><title>" + My Form + "</title></head>\n" +
                        "<h1 align = \"center\">" + My Form + "</h1>\n" +
                        "<ul>\n" +
                        "  <li><b>First Name</b>: "
                        + request.getParameter("first_name") + "\n" +
                        "  <li><b>Last Name</b>: "
                        + request.getParameter("last_name") + "\n" +
                        "</ul>\n" +
                        "</body>"
                "</html>"
        );
    }

    // Method to handle POST method request.
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        doGet(request, response);
    }
}