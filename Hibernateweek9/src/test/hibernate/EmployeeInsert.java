package test.hibernate;

import org.hibernate.Session;

public class EmployeeInsert {

    public static void main(String[] args)
    {
        Session session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();

        //Add new Employee object
        Employee emp = new Employee();
        emp.setID(1);
        emp.setFirstname("Michael");
        emp.setLastname("Clark");

        session.save(emp);

        //Commit the transaction
        session.getTransaction().commit();
        HibernateUtils.shutdown();
    }
}
