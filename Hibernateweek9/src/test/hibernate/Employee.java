package test.hibernate;

import javax.persistence.*;

@Entity
@Table(name = "employee")

public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id")
    private int id;

    @Column(name = "Firstname")
    private String Firstname;

    @Column(name = "Lastname")
    private String Lastname;

    public Employee() {

    }

public int getId() {
        return id;
}

public void setID(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String Firstname) {
        this.Firstname = Firstname;
    }
    public String getLastname() {
        return Lastname;
    }

    public void setLastname(String Lastname) {
        this.Lastname = Lastname;
    }

    public String toString() {
        return Integer.toString(id) + " " + Firstname + " " + Lastname ;
    }
}
