package main;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class json {

    public static String customertoJSON(customer Customer){
        ObjectMapper mapper = new ObjectMapper();
        String wr = "";

        try {
            wr = mapper.writeValueAsString(Customer);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return wr;
    }

    public static customer JSONToCustomer(String wr) {

        ObjectMapper mapper = new ObjectMapper();
        customer Customer = null;

        try {
            Customer = mapper.readValue(wr, customer.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return Customer;
    }

    public static void main(String[] args) {

        customer ct = new customer();
        ct.setName("Michael");
        ct.setPhone("999-999-1111");

        String json = main.json.customertoJSON(ct);
        System.out.println(json);

        customer ct2 = main.json.JSONToCustomer(json);
        System.out.println(ct2);
    }

}
