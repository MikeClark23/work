package com.company;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteTask
{
    // Maximum number of threads in pool
    static final int max_t = 2;

    public static void main(String[] args)
    {
        // creates three tasks
        Main t1 = new Main("task 1");
        Main t2 = new Main("task 2");
        Main t3 = new Main("task 3");



        ExecutorService myService = Executors.newFixedThreadPool(max_t);

        myService.execute(t1);
        myService.execute(t2);
        myService.execute(t3);


        // shutdown
        myService.shutdown();
    }
}
