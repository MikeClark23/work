package com.example.final_project;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;



@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class JSONServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public JSONServlet() {
        super();
    }

    public void destroy() {
        super.destroy();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        JSONArray jsonArray = new JSONArray();
        jsonArray.add("item1");
        jsonArray.add("pencil");
        jsonArray.add("5");
        jsonArray.add(new Double(12.3));
        List<String> list = new ArrayList<String>();
        list.add("item added");
        list.add("item quantity added");
        jsonArray.addAll(list);

        //Page Output Jsonarray content
        PrintWriter out = response.getWriter();
        out.print(jsonArray);
        out.println("======================================");
        for(int i=0;i<jsonArray.size();i++){
            out.print(jsonArray.getString(i));
        }
    }

    public void init() throws ServletException {
    }
}