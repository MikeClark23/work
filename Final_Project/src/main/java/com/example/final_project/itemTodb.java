package com.example.final_project;

import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class itemTodb {

    private static final String url = "jdbc:mysql:Mysql@127.0.0.1:3306:3306/java_demo?useSSL=false";
    private static final String user = "root";
    private static final String password = "Mcdizzle23!";

    private static final String sql = "INSERT INTO item (id, name, quantity) values (?, ?, ?)";

    public <Int> int uploadInfo(Int id, int quantity, String name {
        int row = 0;
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e1) {

            e1.printStackTrace();
        }

        try (Connection connection = DriverManager
                .getConnection(url, user, password);
             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection
                     .prepareStatement(sql)) {

            preparedStatement.setInt(1, id);
            preparedStatement.setInt(2, quantity);
            if (file != null) {
                // fetches input stream of the upload file for the blob column
                preparedStatement.setString(3, name);
            }

            // sends the statement to the database server
            row = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            // process sql exception
            printSQLException(e);
        }
        return row;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

}
