import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    @DisplayName("Add two numbers")
    void add() {
        assertEquals(8, Calculator.add(5, 3));
    }

    @Test
    @DisplayName("Multiply two numbers")
    void multiply() {
        assertEquals(-4, Calculator.multiply(2, -2));
    }
}