package main;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner divide = new Scanner(System.in);
        while (true) {
            System.out.println("Enter two numbers");
            int n1 = divide.nextInt();
            int n2 = divide.nextInt();
            try {
                int answer = div(n1, n2);
                System.out.println("The result is: "+answer);
                break;
            } catch (ArithmeticException e) {
                System.out.println(e.getMessage());
                System.out.println("Try again. Cannot use 0 in second number");
            }
        }
    }

    public static int div(int n1, int n2) throws ArithmeticException {
        return n1 / n2;
    }
}
